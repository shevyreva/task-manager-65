package ru.t1.shevyreva.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.shevyreva.tm.dto.model.AbstractModelDTO;


@Repository
public interface AbstractModelDtoRepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

}
