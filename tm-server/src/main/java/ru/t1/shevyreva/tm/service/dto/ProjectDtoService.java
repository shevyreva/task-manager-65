package ru.t1.shevyreva.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.api.service.dto.IProjectDtoService;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;
import ru.t1.shevyreva.tm.enumerated.ProjectDtoSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.DescriptionEmptyException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.NameEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.repository.dto.ProjectDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public final class ProjectDtoService implements IProjectDtoService {

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);

        add(userId, project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);

        add(userId, project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        projectRepository.saveAndFlush(project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setStatus(status);
        if (project == null) throw new ProjectNotFoundException();

        projectRepository.saveAndFlush(project);
        return project;
    }

    @Nullable
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final ProjectDtoSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        if (sort == null) return projectRepository.findByUserId(userId);
        if (sort.getDisplayName() != null)
            return projectRepository.findAllByUserIdWithSort(userId, getSortType(sort));
        else return null;
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final ProjectDTO project = projectRepository.findOneByUserIdAndId(userId, id);
        if (project == null) throw new TaskNotFoundException();
        return project;
    }

    @NotNull
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        return projectRepository.countByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO removeOne(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ModelNotFoundException();

        projectRepository.deleteOneByIdAndUserId(project.getId(), userId);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        ProjectDTO project = projectRepository.findOneByUserIdAndId(userId, id);
        projectRepository.deleteOneByIdAndUserId(id, userId);
        return project;
    }

    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        projectRepository.deleteByUserId(userId);
    }

    @SneakyThrows
    public List<ProjectDTO> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<ProjectDTO> add(@NotNull final Collection<ProjectDTO> models) {
        if (models == null) throw new ProjectNotFoundException();

        for (@NotNull ProjectDTO project : models) {
            add(project);
        }
        return models;
    }

    @SneakyThrows
    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> models) {
        @Nullable final Collection<ProjectDTO> entities;
        removeAll();
        entities = add(models);
        return entities;
    }

    @SneakyThrows
    @Transactional
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return (projectRepository.findOneByUserIdAndId(userId, id) != null);
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO add(@NotNull final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();

        projectRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO add(@NotNull final String userId, @NotNull final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();

        model.setUserId(userId);
        projectRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    private String getSortType(@NotNull ProjectDtoSort sort) {
        if (sort == ProjectDtoSort.BY_CREATED) return "name";
        else if (sort == ProjectDtoSort.BY_STATUS) return "status";
        else return "created";
    }

}
