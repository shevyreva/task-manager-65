package ru.t1.shevyreva.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.api.service.dto.ISessionDtoService;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.repository.dto.SessionDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public class SessionDtoService implements ISessionDtoService {

    @NotNull
    @Autowired
    private SessionDtoRepository sessionRepository;

    @NotNull
    @SneakyThrows
    @Transactional
    public SessionDTO add(
            @Nullable final String userId,
            @Nullable final SessionDTO session
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        session.setUserId(userId);
        add(session);
        return session;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public SessionDTO add(
            @Nullable final SessionDTO session) {
        sessionRepository.saveAndFlush(session);
        return session;
    }

    @NotNull
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final SessionDTO session = sessionRepository.findByUserIdAndId(userId, id);
        return session;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public SessionDTO removeOne(@Nullable final SessionDTO session) {
        if (session == null) throw new ModelNotFoundException();

        sessionRepository.delete(session);
        return session;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public SessionDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        SessionDTO session = findOneById(userId, id);
        sessionRepository.deleteById(id);
        return session;
    }

    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        sessionRepository.deleteByUserId(userId);
    }

    @SneakyThrows
    public List<SessionDTO> findAll() {
        return sessionRepository.findAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<SessionDTO> add(@NotNull final Collection<SessionDTO> models) {
        if (models == null) throw new ModelNotFoundException();

        for (@NotNull SessionDTO session : models) {
            add(session);
        }
        return models;
    }

    @SneakyThrows
    @Transactional
    public void removeAll() {
        sessionRepository.deleteAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<SessionDTO> set(@NotNull final Collection<SessionDTO> models) {
        @Nullable final Collection<SessionDTO> entities;
        removeAll();
        entities = add(models);
        return entities;
    }

    public boolean existsById(@NotNull final String id) {
        return (sessionRepository.findById(id) != null);
    }

    @NotNull
    public long getSize() {
        @Nullable final long count = sessionRepository.count();
        return count;
    }

}
