package ru.t1.shevyreva.tm.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;
import ru.t1.shevyreva.tm.enumerated.ProjectDtoSort;
import ru.t1.shevyreva.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface IProjectDtoService {

    @NotNull
    @SneakyThrows
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    ProjectDTO create(@Nullable String userId, @Nullable String name);


    @NotNull
    @SneakyThrows
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);


    @NotNull
    @SneakyThrows
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    @SneakyThrows
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable ProjectDtoSort sort);

    @NotNull
    @SneakyThrows
    ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id);

    long getSize(@Nullable final String userId);

    @NotNull
    @SneakyThrows
    ProjectDTO removeOne(@Nullable final String userId, @Nullable final ProjectDTO project);

    @NotNull
    @SneakyThrows
    ProjectDTO removeOneById(@Nullable final String userId, @Nullable final String id);

    void removeAll(@Nullable final String userId);

    @NotNull
    @SneakyThrows
    Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> models);

    @SneakyThrows
    void removeAll();

    @NotNull
    @SneakyThrows
    Collection<ProjectDTO> add(@NotNull final Collection<ProjectDTO> models);

    @NotNull
    @SneakyThrows
    ProjectDTO add(@NotNull final ProjectDTO model);

    @SneakyThrows
    List<ProjectDTO> findAll();

    boolean existsById(@NotNull final String user_id, @NotNull final String id);

}
