package ru.t1.shevyreva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.dto.model.UserDTO;

import java.util.List;

@Repository
public interface UserDtoRepository extends AbstractModelDtoRepository<UserDTO> {

    @Override
    @Transactional
    void deleteAll();

    @Override
    @Nullable List<UserDTO> findAll();

    @Nullable UserDTO findOneById(@NotNull String id);

    @Transactional
    void deleteOneById(@NotNull String id);

    @Override
    long count();

    @Nullable
    UserDTO findByLogin(@NotNull final String login);

    @Nullable
    UserDTO findByEmail(@NotNull final String email);

    @Override
    @Transactional
    void delete(@NotNull final UserDTO user);

}
