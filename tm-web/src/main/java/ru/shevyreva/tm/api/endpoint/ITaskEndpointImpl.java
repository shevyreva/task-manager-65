package ru.shevyreva.tm.api.endpoint;

import ru.shevyreva.tm.model.Task;

import java.util.List;

public interface ITaskEndpointImpl {

    List<Task> findAll();

    Task findById(String id);

    Task save(Task task);

    long count();

    void deleteById(String id);

    void delete(Task task);

}
