package ru.shevyreva.tm.api.endpoint;

import ru.shevyreva.tm.model.Project;

import java.util.List;

public interface IProjectEndpointImpl {

    List<Project> findAll();

    Project findById(String id);

    Project save(Project project);

    long count();

    void deleteById(String id);

    void delete(Project project);

}
