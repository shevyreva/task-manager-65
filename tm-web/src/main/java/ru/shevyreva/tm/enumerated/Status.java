package ru.shevyreva.tm.enumerated;

import lombok.Getter;
import lombok.NonNull;

@Getter
public enum Status {

    NON_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    @NonNull
    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

}
