package ru.shevyreva.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.shevyreva.tm.api.endpoint.IProjectEndpointImpl;
import ru.shevyreva.tm.model.Project;
import ru.shevyreva.tm.service.ProjectService;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements IProjectEndpointImpl {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @GetMapping("/findById/{id}")
    public Project findById(@PathVariable("id") String id) {
        return projectService.findById(id);
    }

    @PostMapping("/save")
    public Project save(@RequestBody Project project) {
        return projectService.save(project);
    }

    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        projectService.deleteById(id);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody Project project) {
        projectService.delete(project);
    }

}
