package ru.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shevyreva.tm.api.repository.IProjectRepository;
import ru.shevyreva.tm.model.Project;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProjectService {
    @Autowired
    IProjectRepository projectRepository;

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findById(@NotNull String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Transactional
    public Project save(@NotNull Project project) {
        return projectRepository.save(project);
    }

    public boolean existById(@NotNull String id) {
        return projectRepository.existsById(id);
    }

    public long count() {
        return projectRepository.count();
    }

    @Transactional
    public void deleteById(@NotNull String id) {
        projectRepository.deleteById(id);
    }

    @Transactional
    public void delete(@NotNull Project project) {
        projectRepository.delete(project);
    }

}
