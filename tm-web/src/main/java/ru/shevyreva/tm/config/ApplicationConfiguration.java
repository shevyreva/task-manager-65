package ru.shevyreva.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.shevyreva.tm")
public class ApplicationConfiguration {
}
