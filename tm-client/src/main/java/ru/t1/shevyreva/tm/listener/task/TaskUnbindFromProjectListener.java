package ru.t1.shevyreva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.task.TaskUnbindToProjectRequest;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Component
public class TaskUnbindFromProjectListener extends AbstractTaskListener {

    @NotNull
    private final String DESCRIPTION = "Unbind task from a project.";

    @NotNull
    private final String NAME = "task-unbind-from-project";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskUnbindFromProjectListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindToProjectRequest request = new TaskUnbindToProjectRequest(getToken(), taskId, projectId);
        taskEndpoint.unbindTaskFromProject(request);
    }

}
