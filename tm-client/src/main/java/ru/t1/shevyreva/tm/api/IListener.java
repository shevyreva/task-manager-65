package ru.t1.shevyreva.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.event.ConsoleEvent;

public interface IListener {

    @Nullable
    public String getName();

    @Nullable
    public String getDescription();

    @Nullable
    public String getArgument();

    void handler(@NotNull final ConsoleEvent consoleEvent);

}
