package ru.t1.shevyreva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.listener.AbstractListener;

import java.util.Collection;

public interface ICommandRepository {

    @Nullable
    AbstractListener getCommandByName(@NotNull String name);

    @Nullable
    AbstractListener getCommandByArgument(@NotNull String argument);

    void add(@NotNull AbstractListener command);

    @NotNull
    Collection<AbstractListener> getTerminalCommands();

    @NotNull
    Iterable<AbstractListener> getCommandWithArgument();

}
